package fr.epf.min.gc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Log.d("debut","demarre");
        home_add_produit_button.setOnClickListener {
            val intent = Intent(this, ScanProduitActivity::class.java)
            startActivity(intent)
        }

        home_list_produits_button.setOnClickListener {
            val intent = Intent(this, ListProduitActivity::class.java)
            startActivity(intent)
        }
    }
}