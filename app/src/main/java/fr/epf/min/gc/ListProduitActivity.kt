package fr.epf.min.gc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import fr.epf.min.gc.data.ProduitDatabase
import fr.epf.min.gc.model.Produit
import kotlinx.android.synthetic.main.activity_list_produit.*
import kotlinx.coroutines.runBlocking


class ListProduitActivity : AppCompatActivity(),ProductCalls.FetchProductsCallBack {

    lateinit var produits : MutableList<Produit>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_produit)
        produits_recyclerview.layoutManager =
            LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        mSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (produits.contains(query)) {
                    Log.d("QUERY2",query)
                    //adapter.filter.filter(query)
                } else {
                    Toast.makeText(this@ListProduitActivity, "No Match found", Toast.LENGTH_LONG).show()
                }
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("QUERY",newText)
                val database = Room
                    .databaseBuilder(baseContext, ProduitDatabase::class.java, "db")
                    .build()
                Log.d("COUCOU","AVANT")
                val ProduitDAO = database.getProduitDAO()

                Log.d("COUCOU","APRES")
                runBlocking {
                    produits = ProduitDAO.getAllProduits().toMutableList()
                    produits_recyclerview.adapter =
                        ProduitAdapter(produits,this@ListProduitActivity)
                    produits=produits.filter{it.nom.contains(newText)}.toMutableList()
                    var total=""
                    produits.forEach{total+=it.nom}
                    Log.d("QUERY",total)
                    produits_recyclerview.adapter = ProduitAdapter(produits,this@ListProduitActivity)
                }
                // adapter.filter.filter(newText)
                return false
            }
        })

        val database = Room
            .databaseBuilder(this, ProduitDatabase::class.java, "db")
            .build()
        /*val ProduitDAO = database.getProduitDAO()
        runBlocking {
            ProduitDAO.addProduit(
                Produit(
                    null,
                    "coucou",
                    "d",
                    "pomme poire",
                    "https://shorturlbot.com/wp-content/uploads/2020/07/ShortURL_TeamsLogo.png",
                )
            );
        }*/

    }

    override fun onStart() {
        super.onStart()
        val database = Room.databaseBuilder(this, ProduitDatabase::class.java, "db").build()
        Log.d("COUCOU","AVANT")
        val ProduitDAO = database.getProduitDAO()

        Log.d("COUCOU","APRES")
        runBlocking {
            produits = ProduitDAO.getAllProduits().toMutableList()
            produits_recyclerview.adapter = ProduitAdapter(produits,this@ListProduitActivity)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_produits, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d("COUCOU","AVANT3")
        when(item.itemId){
            R.id.sync_action -> {
                Log.d("COUCOU","AVANT4")
                synchro()//quand on clique sur le bouton on scanne
               }
            R.id.add_client_action->{
                AlertDialog.Builder(this)
                    .setTitle("Supprimer produit")
                    .setMessage("Etes-vous sûr ?")
                    .setPositiveButton("Oui") { _, _ ->
                        val database = Room
                            .databaseBuilder(this, ProduitDatabase::class.java, "db")
                            .build()
                        Log.d("COUCOU","AVANT")
                        val ProduitDAO = database.getProduitDAO()
                        runBlocking {
                            produits.map{ProduitDAO.deleteProduit(it)
                            }
                        }
                        finish()
                        Toast.makeText(this, "Produit supprimé", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButton("Non"){ dialog,_ ->
                        dialog.dismiss()
                    }
                    .show()
                        }

        }
        return super.onOptionsItemSelected(item);
    }

    private fun synchro() {
        val intent= Intent(applicationContext, ScanProduitActivity::class.java)
        startActivity(intent)

    }

    override fun fetchProductsResponse(produit: GetProductsResult?) {
        Log.d("coucou","coucou"+produit?.product!!.generic_name)
        val database = Room
            .databaseBuilder(this, ProduitDatabase::class.java, "db")
            .build()
        val ProduitDAO = database.getProduitDAO()
        runBlocking {
            ProduitDAO.addProduit(
                Produit(
                    null,
                    produit!!.product.generic_name,
                    ""+produit!!.product.nutriscore_grade,
                    produit.product.ingredients.map{it.text}.joinToString(","),
                    produit.product.image_front_small_url,
                )
            );
        }
        }

    override fun fetchProductsFailure(t: Throwable?) {
        Log.d("COUCOU","DEDANS4"+t)
    }
}
