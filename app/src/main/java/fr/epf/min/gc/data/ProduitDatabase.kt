package fr.epf.min.gc.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import fr.epf.min.gc.model.Produit
import fr.epf.min.gc.model.Gender

@Database(entities = [Produit::class], version = 1)
abstract class ProduitDatabase : RoomDatabase(){
    abstract fun getProduitDAO() : ProduitDAO
}