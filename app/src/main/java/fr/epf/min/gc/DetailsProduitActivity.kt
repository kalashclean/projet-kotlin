package fr.epf.min.gc

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.room.Room
import com.bumptech.glide.Glide
import fr.epf.min.gc.data.ProduitDatabase
import fr.epf.min.gc.model.Produit
import fr.epf.min.gc.model.Gender
import kotlinx.android.synthetic.main.activity_details_produit.*
import kotlinx.android.synthetic.main.produit_view.view.*
import kotlinx.coroutines.runBlocking

class DetailsProduitActivity : AppCompatActivity() {

    var produit:Produit?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_produit)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        produit = intent.getParcelableExtra<Produit>("id")

//        val client = Client.all()[id]
        produit_nutriscore_imageview.setImageResource(getResources().getIdentifier("nutriscore_"+produit?.nutriscore, "drawable", getPackageName()))
        produit_lastname_textview.text = "nom:"+produit?.nom+ "\n ingredients: "+produit?.ingredients
        produit_firstname_textview.text = produit?.nutriscore
        Glide.with(this).load(produit?.image).into(produit_details_imageview)

//        produit_details_imageview.setImageResource(produit!!.getPicture())
//        produit_details_imageview.setImageBitmap(produit?.getPicture(produit?.image))

        produit_details_imageview.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 12334)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_produit,menu)
        return super.onPrepareOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.delete_produit_action -> {
                AlertDialog.Builder(this)
                    .setTitle("Supprimer produit")
                    .setMessage("Etes-vous sûr ?")
                    .setPositiveButton("Oui") { _, _ ->
                        val database = Room
                            .databaseBuilder(this, ProduitDatabase::class.java, "db")
                            .build()
                        Log.d("COUCOU","AVANT")
                        val ProduitDAO = database.getProduitDAO()
                        runBlocking {
                            ProduitDAO.deleteProduit(produit)
                        }
                        finish()
                        Toast.makeText(this, "Produit supprimé", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButton("Non"){ dialog,_ ->
                        dialog.dismiss()
                    }
                    .show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 12334){
            val bitmap = data?.getParcelableExtra("data") as? Bitmap
            produit_details_imageview.setImageBitmap(bitmap)
        }

    }
}


fun Produit.getPicture(url:String?):Bitmap {
    val image = java.net.URL(url).openStream()
    val bitmap:Bitmap = BitmapFactory.decodeStream(image)
    return bitmap
}