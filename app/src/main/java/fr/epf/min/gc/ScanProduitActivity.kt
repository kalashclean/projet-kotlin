package fr.epf.min.gc

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.room.Room
import com.google.zxing.Result;
import fr.epf.min.gc.data.ProductService
import fr.epf.min.gc.data.ProduitDAO
import fr.epf.min.gc.data.ProduitDatabase
import fr.epf.min.gc.model.Produit
import kotlinx.coroutines.runBlocking
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ScanProduitActivity : Activity(), ZXingScannerView.ResultHandler,ProductCalls.FetchProductsCallBack{
    private var mScannerView: ZXingScannerView? = null
    public override fun onCreate(state: Bundle?) {
        ActivityCompat.requestPermissions(this, arrayOf( Manifest.permission.CAMERA), 100);
        super.onCreate(state)
        mScannerView = ZXingScannerView(this)
        setContentView(mScannerView)
    }

    public override fun onResume() {
        super.onResume()
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera() // Stop camera on pause
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        Log.v("SCAN", rawResult.getText()) // Prints scan results
        Log.v(
            "SCAN",
            rawResult.getBarcodeFormat().toString()
        ) // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        //mScannerView?.resumeCameraPreview(this)
        //synchro(rawResult.getText())
        ProductCalls.fetchProducts(this,rawResult.text);//code
    }
    private fun synchro(code:String) {
        // ProductCalls.fetchProducts(this,"3092718621681");//code
        val retrofit = Retrofit.Builder()
            .baseUrl("https://world.openfoodfacts.org/api/v0/product/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val service = retrofit.create(ProductService::class.java)
        val database = Room
            .databaseBuilder(this, ProduitDatabase::class.java, "db")
            .build()
        val ProduitDAO = database.getProduitDAO()
        runBlocking {
                val result = service.getClients(code);
            Log.d("EPF", "$result")
            val results = result.results
                ProduitDAO.addProduit(Produit(
                    null,
                    results.generic_name,
                    ""+results.nutriscore_grade ,
                    results.ingredients.map{it.text}.joinToString(separator=","),
                    results.image_front_small_url,
                ));
            }
        }
    override fun fetchProductsResponse(produit: GetProductsResult?) {
        Log.d("coucou","coucou"+produit?.product!!.generic_name)
        val database = Room
            .databaseBuilder(this, ProduitDatabase::class.java, "db")
            .build()
        val ProduitDAO = database.getProduitDAO()
        runBlocking {
            val Produits= Produit(
                null,
                produit!!.product.generic_name,
                ""+produit!!.product.nutriscore_grade,
                produit.product.ingredients.map{it.text}.joinToString(","),
                produit.product.image_front_small_url,
            )
            ProduitDAO.addProduit(
                Produits
            );
            val intent= Intent(applicationContext, DetailsProduitActivity::class.java)
            intent.putExtra("id", Produits)
            startActivity(intent)
        }

    }


    override fun fetchProductsFailure(t: Throwable?) {
        Log.d("COUCOU","DEDANS4"+t)
    }

}