package fr.epf.min.gc.data
import fr.epf.min.gc.model.Produit
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Query

interface ProductService {
    //---USUAL RETROFIT
    @GET("/{code}")
    fun getProducts(@Path("code") code:String): Call<String?>?
    @GET("/{code}")
    suspend fun getClients(@Path("code") results : String) : GetProductsResult

    companion object {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://world.openfoodfacts.org/api/v0/product/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
    }
}

data class GetProductsResult(val results: Product)
data class Product(val generic_name:String,val nutriscore_grade:Int,val ingredients:List<Ingredient>,val image_front_small_url:String)
data class Ingredient(val text:String)
