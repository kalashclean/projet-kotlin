package fr.epf.min.gc.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import fr.epf.min.gc.model.Produit

@Dao
interface ProduitDAO {

    @Query("select * from produits")
    suspend fun getAllProduits() : List<Produit>

    @Insert
    suspend fun addProduit(produit: Produit)

    @Delete
    suspend fun deleteProduit(produit: Produit?)

}
