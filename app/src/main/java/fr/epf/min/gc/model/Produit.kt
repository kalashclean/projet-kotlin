package fr.epf.min.gc.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

enum class Gender {
    Man, Woman
}

@Entity(tableName = "produits")
@Parcelize
data class Produit(

    @PrimaryKey() val id: Int?,
    val nom: String,
    val nutriscore: String,
    val ingredients: String,
    val image: String
) : Parcelable {

}
