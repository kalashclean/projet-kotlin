package fr.epf.min.gc

import android.util.Log
import com.google.gson.GsonBuilder
import fr.epf.min.gc.data.ProductService
import fr.epf.min.gc.model.Produit
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.ref.WeakReference


object ProductCalls {
    fun fetchProducts(callBack: FetchProductsCallBack?,code:String) {
        println("Attempting to Fetch JSON")

        val url = "https://world.openfoodfacts.org/api/v0/product/"+code

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: okhttp3.Callback {
            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                val callBackWeakReference = WeakReference(callBack)
                val body = response?.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val homeFeed:GetProductsResult = gson.fromJson(body, GetProductsResult::class.java)
                if (callBackWeakReference.get() != null) {
                    callBackWeakReference.get()!!.fetchProductsResponse(homeFeed)
                }

            }

            override fun onFailure(call: okhttp3.Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }

    interface FetchProductsCallBack {
        fun fetchProductsResponse(produit: GetProductsResult?)
        fun fetchProductsFailure(t: Throwable?)
    }
}
data class GetProductsResult(val product: Product)
data class Product(val generic_name:String,val nutriscore_grade:Int,val ingredients:List<Ingredient>,val image_front_small_url:String)
data class Ingredient(val text:String)

class HomeFeed(val videos: List<Video>)

class Video(val id: Int, val name: String, val link: String, val imageUrl: String,
            val numberOfViews: Int, val channel: Channel)

class Channel(val name: String, val profileImageUrl: String)