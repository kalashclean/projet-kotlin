package fr.epf.min.gc

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.epf.min.gc.model.Produit
import kotlinx.android.synthetic.main.activity_details_produit.*
import kotlinx.android.synthetic.main.produit_view.*
import kotlinx.android.synthetic.main.produit_view.view.*
import kotlinx.coroutines.CoroutineScope

class ProduitAdapter(val produits: List<Produit>, val context: Context) : RecyclerView.Adapter<ProduitAdapter.ProduitViewHolder>() {

    class ProduitViewHolder(val produitView : View) : RecyclerView.ViewHolder(produitView)

    override fun getItemCount()  = produits.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProduitViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View =
            inflater.inflate(R.layout.produit_view, parent, false)
        return ProduitViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProduitViewHolder, position: Int) {
        val produit = produits[position]
        holder.produitView.produit_name_textview.text = "Libellé : ${produit.nom} nutriscore : ${produit.nutriscore}"
        val image = holder.produitView.produit_imageview
        Glide.with(context).load(produit.image).into(image)

        holder.produitView.setOnClickListener {
            val intent = Intent(it.context, DetailsProduitActivity::class.java)
                intent.putExtra("id", produit)
            it.context.startActivity(intent)
        }

    }




}